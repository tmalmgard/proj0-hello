# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms in CIS 322 at University of Oregon

The program prints "Hello World"

Author: Torsten Malmgard
Mail: torstenm@uoregon.edu
